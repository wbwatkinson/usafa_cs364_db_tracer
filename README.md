# Tracer Bullet Connecting Angular to MySQL/MongoDB Technologies

* MongoDB/MySQL
* Express
* Angular
* Node

*Requires MongoDB and MySQL server running*

## Initialize

* Run `initialize.bat` to resolve dependencies and to seed databases
* If you don't want to run both *MongoDB* and *MySQL* servers, comment out appropriate lines in `initialize.bat` using REM

## Run the application
`npm start <port> <mongo/mysql>`

The application will default to port 3000 and "mongo" without command line input

## Modularization

When the controller and model are separated appropriately in this example, you can push the database technology decision all the way to the model. Note these lines of code in **server/controller/users.js**:

```javascript
var dboption = process.argv[3] || "mongo";

if (dboption === "mongo") {
  Users = require(path.join(__dirname,'..','models_mongo','users'));
} else {
  Users = require(path.join(__dirname,'..','models_mysql','users'));
}
```

With a command line option, you can select which database is used.  Apart from **models_mongo/users.js** vs **models_mysql/users.js** all other code is identical

## Protection against SQL Injection

### MongoDB

Many of the common SQL Injection attacks are defeated in *MongoDB* since with mongo queries we are no longer dealing with a query language in the form of a string. For example, a mongo query might look something like this:

`db.users.find({username: username});`

It is possible to send non-strings to the query that will result in an injection, but that is beyond the scope of this course. You could verify that the value for username is actually a string, which has been done in **models_mongo/users.js**

### MySQL

The key to preventing SQL injection in MySQL is to not execute a query string directly on the database. The **.query** method provided handles this for you. Note the query in **models_mysql/users.js**. The query is written with '?' as placeholders, allowing the user input to be passed into the **.query** method separately. In this way, the **.query** method will escape the parameters, preventing a SQL Injection.