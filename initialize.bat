REM install node packages
call npm install

REM npm install turns ECHO OFF, so commands are no longer visible
ECHO ON

REM create MongoDB database mydb, collection users, and load data from file
mongoimport -v --db mydb --collection users --type json --file server/seed/users-seed.json --jsonArray --drop

REM create MySQL Database and load with data
mysql -v -u root < server\seed\mydb-schema.sql
mysql -v -u root mydb < server\seed\mydb-data.sql


