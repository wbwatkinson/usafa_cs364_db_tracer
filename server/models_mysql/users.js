/**
 * Created by Warren.Watkinson on 4/16/2016.
 */

var path = require('path');
var mongo = require('mysql');
var mysqlUtil = require(path.join(__dirname,'..','services','mysqlUtil'));

// Connect to MySQL
mysqlUtil.connect();

var Users = {
  getAll: function(callback) {
    var connection = mysqlUtil.connect();
    var sql = 'SELECT * FROM user';
    connection.query(sql,function(err,results) {
      callback(err,results);
    });
  },
  
  findByUserName: function (username,callback) {
    var connection = mysqlUtil.connect();
    var sql = 'SELECT * FROM user WHERE username = ?';
    connection.query(sql,[username],function(err,results) {
      console.log(err);
      callback(err,results);
    });
  }
};

module.exports = Users;