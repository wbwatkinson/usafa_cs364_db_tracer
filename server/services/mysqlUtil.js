/**
 * Created by Warren.Watkinson on 4/13/2016.
 */

var mysql = require('mysql');
var pool = mysql.Pool;

exports.connect = function() {
  var d = new Date();
  if (pool) {return pool}
    
  pool = mysql.createPool ({
      host     : 'localhost',
      user     : 'root',
      password : '',
      database : 'mydb'
    });
  
  console.log(d.toUTCString() + ': MySQL connection established');
  
  return pool;
};

    
/*  
  pool.createPool({
    
  })
}

createPool({
  host      : 'localhost',
  user      : 'root',
  password  : '',
  database  : 'swapmeet'
});

exports.users = function(callback) {
  var sql = 'SELECT * FROM user';
  pool.getConnection(function(err, connection) {
    if(err) {
      console.log(err);
      callback(true);
      return;
    }
    connection.query(sql,function(err,results) {
    connection.release();
    if(err) {
      console.log(err);
      callback(true);
      return; }
      callback(false, results);
    });
  });
};

exports.getUserData = function(username, callback) {
  var sql = 'SELECT * FROM user WHERE username = ?';
  pool.getConnection(function(err, connection) {
    if(err) {
      console.log(err);
      callback(true);
      return;
    }
    connection.query(sql,[username],function(err,results) {
      connection.release();
      if(err) {
        console.log(err);
        callback(true);
        return; }
      callback(false, results);
    });
  });
};

  */